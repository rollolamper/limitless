<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'c26_limitless');

/** Имя пользователя MySQL */
define('DB_USER', 'c26_limitless');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'CoLbCS5rv!6b');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'AuO|yysv}T$V*0CoCj_traCeR q<&z|XBgn?`oF@gc{byp3-((9[Gk3Pe}FPU;@|');
define('SECURE_AUTH_KEY',  '?<rPJN:~rWaz![YrH5xk+m5CEn~nE4g7P/wdMB,,Y@[6OhIy`w%>*[Ho&$fhZ:ne');
define('LOGGED_IN_KEY',    ')UKbTDr9_J`Mt!O6$#:H-?UAPD9cmv*#Ix|q_8qk$x+vBN}ZAy+Taf.G:Pp|W;X.');
define('NONCE_KEY',        't~YP~|#F65kR0yAV+T-TI@4#^7D@v<Dl)z%e5rAjc>B@V!oxB0N4u^^1Uju8hq0V');
define('AUTH_SALT',        'kSkC2:)1$xh=]W#!hCJc_~qrmVSaL>ud,VR4gr(TF`g!F[Y0wfW4p_}fMn|0 fTP');
define('SECURE_AUTH_SALT', ' L+Is9m.H@>a4|.+osM<!2U_U-5 -x?/mM^(#x?8DAsbqm5y0!ckNf8]=3!Kn@z_');
define('LOGGED_IN_SALT',   'DrpEG5M]sg5$pty3l^)p1R%NC?$#HR9{<JcEo @q;D{6 [P8@=1(#2CbsuG%,*k(');
define('NONCE_SALT',       'Htiawmo~J7J52v ZEY4gLm`Yihy`.4J{+Kp!I3EhR8@nMYiX-,x ^,rmBvw3(m26');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 * 
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
